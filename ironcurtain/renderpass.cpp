#include "pch.hpp"
#include "renderpass.hpp"


RenderPass::RenderPass(std::shared_ptr<vk::Device> pDevice, vk::Format swapChainImageFormat)
    : m_pDevice(pDevice)
{
    const auto colorAttachment = vk::AttachmentDescription()
        .setFormat(swapChainImageFormat)
        .setSamples(vk::SampleCountFlagBits::e1)

        .setLoadOp(vk::AttachmentLoadOp::eClear)
        .setStoreOp(vk::AttachmentStoreOp::eStore)

        .setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
        .setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)

        .setInitialLayout(vk::ImageLayout::eUndefined)
        .setFinalLayout(vk::ImageLayout::ePresentSrcKHR);

    const auto colorAttachmentRef = vk::AttachmentReference()
        .setAttachment(0)
        .setLayout(vk::ImageLayout::eColorAttachmentOptimal);

    const auto subpass = vk::SubpassDescription()
        .setPipelineBindPoint(vk::PipelineBindPoint::eGraphics)

        .setColorAttachmentCount(1)
        .setPColorAttachments(&colorAttachmentRef);

    const auto subpassDependency = vk::SubpassDependency()
        .setSrcSubpass(VK_SUBPASS_EXTERNAL)
        .setDstSubpass(0)

        .setSrcStageMask(vk::PipelineStageFlagBits::eColorAttachmentOutput)
        .setDstStageMask(vk::PipelineStageFlagBits::eColorAttachmentOutput)

        .setSrcAccessMask({})
        .setDstAccessMask(vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite);

    const auto renderPassInfo = vk::RenderPassCreateInfo()
        .setAttachmentCount(1)
        .setPAttachments(&colorAttachment)

        .setSubpassCount(1)
        .setPSubpasses(&subpass)

        .setDependencyCount(1)
        .setPDependencies(&subpassDependency);

    m_vkHandle = m_pDevice->createRenderPass(renderPassInfo);
}

RenderPass::~RenderPass()
{
    m_pDevice->destroyRenderPass(m_vkHandle);
}
