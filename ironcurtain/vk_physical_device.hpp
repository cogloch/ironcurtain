#pragma once
#include <vulkan/vulkan.hpp>
#include "vk_utils.hpp"


class VulkanPhysicalDevice
{
public:
    VulkanPhysicalDevice(const std::vector<vk::PhysicalDevice>& existingDevices, vk::SurfaceKHR surface);

    vk::PhysicalDevice GetHandle() const;
    u32 FindMemoryType(u32 typeFilter, vk::MemoryPropertyFlags properties) const;
    QueueFamilyIndices GetQueueFamilyIndices() const;

    vk::Device CreateLogicalDevice(const std::vector<vk::DeviceQueueCreateInfo>& queueCreateInfos) const;

private:
    static const std::vector<const char*> s_deviceExtensions; 
 
    bool IsSuitable(vk::PhysicalDevice device, vk::SurfaceKHR surface);
    bool SupportsExtensions(vk::PhysicalDevice device);

    vk::PhysicalDevice m_vkHandle;
    QueueFamilyIndices m_queueFamilyIndices;
    vk::PhysicalDeviceMemoryProperties m_memoryProperties;
};