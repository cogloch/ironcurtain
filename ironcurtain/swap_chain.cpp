#include "pch.hpp"
#include "swap_chain.hpp"
#include "vk_utils.hpp"
#include <GLFW/glfw3.h>


SwapChain::SwapChain(std::shared_ptr<vk::Device> logicalDevice, 
                     vk::PhysicalDevice physDevice, vk::SurfaceKHR surface, GLFWwindow* window)
    : m_pDevice(logicalDevice)
{
    int width = 0, height = 0;
    glfwGetFramebufferSize(window, &width, &height);
    Create(physDevice, surface, width, height);
    CreateImageViews();
}

SwapChain::~SwapChain()
{
    for (auto& imageView : m_imageViews)
        m_pDevice->destroyImageView(imageView);

    m_pDevice->destroySwapchainKHR(m_vkHandle);
}

#pragma region Region:: Choose Surface Props
vk::SurfaceFormatKHR SwapChain::ChooseSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats)
{
    if (availableFormats.size() == 1
     && availableFormats[0].format == vk::Format::eUndefined)
    {
        return { vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear };
    }

    for (const auto& format : availableFormats)
    {
        if (format.format == vk::Format::eB8G8R8A8Unorm
            && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
        {
            return format;
        }
    }

    return availableFormats[0];
}

vk::PresentModeKHR SwapChain::ChoosePresentMode(const std::vector<vk::PresentModeKHR>& availableModes)
{
    auto bestMode = vk::PresentModeKHR::eFifo;

    for (const auto& mode : availableModes)
    {
        if (mode == vk::PresentModeKHR::eMailbox)
        {
            return mode;
        }
        else if (mode == vk::PresentModeKHR::eImmediate)
        {
            bestMode = mode;
        }
    }

    return bestMode;
}

vk::Extent2D SwapChain::ChooseExtent(const vk::SurfaceCapabilitiesKHR& capabilities, u32 windowWidth, u32 windowHeight)
{
    if (capabilities.currentExtent.width != std::numeric_limits<u32>::max())
    {
        return capabilities.currentExtent;
    }
    else
    {
        VkExtent2D actualExtent = { windowWidth, windowHeight };

        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}
#pragma endregion 

void SwapChain::Create(vk::PhysicalDevice physDevice, vk::SurfaceKHR surface, u32 windowWidth, u32 windowHeight)
{
    // Query surface support
    const auto surfaceFormats = physDevice.getSurfaceFormatsKHR(surface);
    const auto surfacePresentModes = physDevice.getSurfacePresentModesKHR(surface);
    const auto surfaceCaps = physDevice.getSurfaceCapabilitiesKHR(surface);

    // Choose desirable surface props 
    const auto surfaceFormat = ChooseSurfaceFormat(surfaceFormats);
    const auto presentMode = ChoosePresentMode(surfacePresentModes);
    const auto extent = ChooseExtent(surfaceCaps, windowWidth, windowHeight);

    const u32 imageCount = std::min(surfaceCaps.minImageCount + 1, surfaceCaps.maxImageCount);

    vk::SharingMode imageSharingMode = vk::SharingMode::eExclusive;
    auto indices = QueueFamilyIndices::FindQueueFamilies(physDevice, surface);
    std::vector<u32> queueFamilyIndices;

    if (indices.graphicsFamily != indices.presentFamily)
    {
        imageSharingMode = vk::SharingMode::eConcurrent;
        queueFamilyIndices.push_back(indices.graphicsFamily.value());
        queueFamilyIndices.push_back(indices.presentFamily.value());
    }

    const auto createInfo = vk::SwapchainCreateInfoKHR()
        .setSurface(surface)
        .setMinImageCount(imageCount)
        .setImageFormat(surfaceFormat.format)
        .setImageColorSpace(surfaceFormat.colorSpace)
        .setImageExtent(extent)
        .setImageArrayLayers(1)
        .setImageUsage(vk::ImageUsageFlagBits::eColorAttachment)
        .setImageSharingMode(imageSharingMode)
        .setQueueFamilyIndexCount(queueFamilyIndices.size())
        .setPQueueFamilyIndices(queueFamilyIndices.data())
        .setPreTransform(surfaceCaps.currentTransform)
        .setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque)
        .setPresentMode(presentMode)
        .setClipped(true)
        .setOldSwapchain(nullptr);

    m_vkHandle = m_pDevice->createSwapchainKHR(createInfo);
    m_images = m_pDevice->getSwapchainImagesKHR(m_vkHandle);

    m_imageFormat = surfaceFormat.format;
    m_extent = extent;
}

void SwapChain::CreateImageViews()
{
    m_imageViews.reserve(m_images.size());
    for (auto& image : m_images)
    {
        const auto subresourceRange = vk::ImageSubresourceRange()
            .setAspectMask(vk::ImageAspectFlagBits::eColor)
            .setBaseMipLevel(0)
            .setLevelCount(1)
            .setBaseArrayLayer(0)
            .setLayerCount(1);

        const auto createInfo = vk::ImageViewCreateInfo()
            .setImage(image)
            .setViewType(vk::ImageViewType::e2D)
            .setFormat(m_imageFormat)
            .setSubresourceRange(subresourceRange);

        m_imageViews.push_back(m_pDevice->createImageView(createInfo));
    }
}
