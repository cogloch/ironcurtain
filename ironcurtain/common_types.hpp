#pragma once
#include <glm/glm.hpp>


using u32 = std::uint32_t;
using u64 = std::uint64_t;

template<typename UnitClass>
struct Value
{
    long double value;
    Value& operator+= (Value other)
    {
        value += other.value;
        return *this;
    }
};

template<typename UnitClass>
Value<UnitClass> operator+ (Value<UnitClass> lhs, Value<UnitClass> rhs) { return lhs += rhs; }

// Dummy 
struct D;
using Distance = Value<D>;

Distance operator""_m (long double d) { return { d }; }
Distance operator""_km(long double d) { return { d * 1000.0 }; }
