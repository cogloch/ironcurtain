#pragma once
#include <vulkan/vulkan.hpp>


// Swapchain dependent. 
// Recreate if swapchain is recreated. 
class RenderPass
{
public:
    RenderPass(std::shared_ptr<vk::Device> pDevice,
               vk::Format swapChainImageFormat);
    ~RenderPass();
    vk::RenderPass GetHandle() const { return m_vkHandle; }

private:
    vk::RenderPass m_vkHandle;
    std::shared_ptr<vk::Device> m_pDevice;
};