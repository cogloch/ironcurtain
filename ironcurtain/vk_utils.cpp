#include "pch.hpp"
#include "vk_utils.hpp"


QueueFamilyIndices QueueFamilyIndices::FindQueueFamilies(const vk::PhysicalDevice& physDevice, vk::SurfaceKHR surface)
{
    QueueFamilyIndices indices;

    const auto queueFamilies = physDevice.getQueueFamilyProperties();
    int i = 0;
    for (const auto& queueFamily : queueFamilies)
    {
        if (queueFamily.queueCount > 0
            && queueFamily.queueFlags & vk::QueueFlagBits::eGraphics)
        {
            indices.graphicsFamily = i;
        }

        if (queueFamily.queueCount > 0
            && physDevice.getSurfaceSupportKHR(i, surface))
        {
            indices.presentFamily = i;
        }

        if (indices.IsComplete())
        {
            break;
        }

        i++;
    }

    return indices;
}
