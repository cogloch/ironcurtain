#include "pch.hpp"
#include "vk_physical_device.hpp"
#include "vk_utils.hpp"


const std::vector<const char*> VulkanPhysicalDevice::s_deviceExtensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

VulkanPhysicalDevice::VulkanPhysicalDevice(const std::vector<vk::PhysicalDevice>& existingDevices, vk::SurfaceKHR surface)
{
    bool foundSuitable = false;
    for (const auto& device : existingDevices)
    {
        if (IsSuitable(device, surface))
        {
            m_vkHandle = device;
            foundSuitable = true;
            break;
        }
    }

    if (!foundSuitable)
        throw std::runtime_error("Failed to find suitable GPU!");

    m_memoryProperties = m_vkHandle.getMemoryProperties();
    m_queueFamilyIndices = QueueFamilyIndices::FindQueueFamilies(m_vkHandle, surface);
}

vk::PhysicalDevice VulkanPhysicalDevice::GetHandle() const
{
    return m_vkHandle;
}

u32 VulkanPhysicalDevice::FindMemoryType(u32 typeFilter, vk::MemoryPropertyFlags properties) const
{
    for (u32 i = 0; i < m_memoryProperties.memoryTypeCount; ++i)
        if ((typeFilter & (1 << i)) &&
            (m_memoryProperties.memoryTypes[i].propertyFlags & properties) == properties)
            return i;

    throw std::runtime_error("Failed to find suitable memory type!");
}

QueueFamilyIndices VulkanPhysicalDevice::GetQueueFamilyIndices() const
{
    return m_queueFamilyIndices;
}

vk::Device VulkanPhysicalDevice::CreateLogicalDevice(const std::vector<vk::DeviceQueueCreateInfo>& queueCreateInfos) const
{
    return m_vkHandle.createDevice(vk::DeviceCreateInfo()
        .setQueueCreateInfoCount(queueCreateInfos.size())
        .setPQueueCreateInfos(queueCreateInfos.data())
        .setEnabledExtensionCount(s_deviceExtensions.size())
        .setPpEnabledExtensionNames(s_deviceExtensions.data())
    );
}

bool VulkanPhysicalDevice::IsSuitable(vk::PhysicalDevice device, vk::SurfaceKHR surface)
{
    const auto properties = device.getProperties();
    const auto features = device.getFeatures();
    const auto queueFamilies = QueueFamilyIndices::FindQueueFamilies(device, surface);

    bool extensionsOk = SupportsExtensions(device);

    bool swapChainOk = false;
    if (extensionsOk)
    {
        const auto surfaceFormats = device.getSurfaceFormatsKHR(surface);
        const auto presentModes = device.getSurfacePresentModesKHR(surface);
        swapChainOk = !surfaceFormats.empty() && !presentModes.empty();
    }

    return properties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu
        && features.geometryShader
        && queueFamilies.IsComplete()
        && extensionsOk
        && swapChainOk;
}

bool VulkanPhysicalDevice::SupportsExtensions(vk::PhysicalDevice device)
{
    const auto availableExts = device.enumerateDeviceExtensionProperties();
    std::set<std::string> requiredExts(s_deviceExtensions.begin(), s_deviceExtensions.end());
    for (const auto& ext : availableExts)
    {
        requiredExts.erase(ext.extensionName);
    }
    return requiredExts.empty();
}
