#pragma once
#include <vulkan/vulkan.hpp>


struct GLFWwindow;

class SwapChain
{
public:
    explicit SwapChain(std::shared_ptr<vk::Device> logicalDevice, 
                       vk::PhysicalDevice physDevice, vk::SurfaceKHR surface, GLFWwindow* window);
    ~SwapChain();

    vk::SwapchainKHR GetHandle() const { return m_vkHandle; }
    vk::Format GetImageFormat() const { return m_imageFormat; }
    vk::ImageView GetImageViewAtIndex(size_t index) const { return m_imageViews[index]; }
    vk::Extent2D GetExtent() const { return m_extent; }
    u32 GetExtentWidth() const { return m_extent.width; }
    u32 GetExtentHeight() const { return m_extent.height; }
    size_t GetImageCount() const { return m_imageViews.size(); }

private:
    // Choose surface props
    static vk::SurfaceFormatKHR ChooseSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats);
    static vk::PresentModeKHR ChoosePresentMode(const std::vector<vk::PresentModeKHR>& availableModes);
    static vk::Extent2D ChooseExtent(const vk::SurfaceCapabilitiesKHR& capabilities, u32 windowWidth, u32 windowHeight);

private:
    void Create(vk::PhysicalDevice physDevice, vk::SurfaceKHR surface, u32 windowWidth, u32 windowHeight);
    void CreateImageViews();
    
    std::shared_ptr<vk::Device> m_pDevice;

    vk::SwapchainKHR m_vkHandle;
    std::vector<vk::Image> m_images;
    std::vector<vk::ImageView> m_imageViews;
    vk::Format m_imageFormat;
    vk::Extent2D m_extent;
};