#include "pch.hpp"
#include "vk_app.hpp"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>


std::vector<char> ReadFile(const std::string& filename)
{
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if (!file.is_open()) 
    {
        throw std::runtime_error("Read File:: Failed to open file!");
    }

    size_t fileSize = (size_t)file.tellg();
    std::vector<char> buffer(fileSize);
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    return buffer;
}

Application::Application()
{
    InitWindow();
    InitVulkan();
    RunLoop();
    Cleanup();
}

void Application::InitWindow()
{
    glfwInit();
    
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    //glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    m_pWindow = glfwCreateWindow(m_windowWidth, m_windowHeight, "Iron Curtain", nullptr, nullptr);
    
    glfwSetWindowAspectRatio(m_pWindow, 4, 3);
    glfwSetWindowUserPointer(m_pWindow, this);

    // Event callbacks 
    //glfwSetWindowIconifyCallback(m_pWindow, OnWindowMinimize);
    glfwSetFramebufferSizeCallback(m_pWindow, OnWindowFramebufferResize);
}

void Application::InitVulkan()
{
    m_instance = std::make_unique<VulkanInstance>();
    m_surface = m_instance->GetSurface(m_pWindow);
    m_physicalDevice = std::make_unique<VulkanPhysicalDevice>(m_instance->EnumeratePhysicalDevices(), m_surface);
    CreateLogicalDevice();

    // Swapchain and dependent objects 
    m_swapChain = std::unique_ptr<SwapChain>(new SwapChain(m_device, m_physicalDevice->GetHandle(), m_surface, m_pWindow));
    m_renderPass = std::make_unique<RenderPass>(m_device, m_swapChain->GetImageFormat());
    CreateDescriptorSetLayout();
    CreateGraphicsPipeline();
    CreateFramebuffers();
    
    CreateCmdPool();

    CreateVertexBuffer();
    CreateIndexBuffer();

    CreateUniformBuffers();
    CreateDescriptorPool();
    CreateDescriptorSets();

    CreateCmdBuffers();

    CreateSyncObjects();
}

void Application::CreateDescriptorSetLayout()
{
    const auto uboLayoutBinding = vk::DescriptorSetLayoutBinding()
        .setBinding(0)
        .setDescriptorType(vk::DescriptorType::eUniformBuffer)
        .setDescriptorCount(1)
        .setStageFlags(vk::ShaderStageFlagBits::eVertex);

    const auto layoutInfo = vk::DescriptorSetLayoutCreateInfo()
        .setBindingCount(1)
        .setPBindings(&uboLayoutBinding);

    m_descriptorSetLayout = m_device->createDescriptorSetLayout(layoutInfo);
}


namespace Pipeline
{
    std::vector<vk::VertexInputBindingDescription> vertexBindings;
    std::vector<vk::VertexInputAttributeDescription> vertexAttributes;
    vk::Viewport viewport;
    vk::Rect2D scissor;
    vk::PipelineColorBlendAttachmentState colorBlendAttachment;

    vk::PipelineVertexInputStateCreateInfo CreateVertexInputState()
    {
        Pipeline::vertexBindings = { Vertex::GetBindingDescription() };
        const auto attributeDescriptionsArray = Vertex::GetAttributeDescriptions();
        Pipeline::vertexAttributes = std::vector<vk::VertexInputAttributeDescription>(attributeDescriptionsArray.begin(), attributeDescriptionsArray.end());
        return vk::PipelineVertexInputStateCreateInfo()
            .setVertexBindingDescriptionCount((u32)vertexBindings.size())
            .setPVertexBindingDescriptions(vertexBindings.data())
            .setVertexAttributeDescriptionCount((u32)vertexAttributes.size())
            .setPVertexAttributeDescriptions(vertexAttributes.data());
    }

    vk::PipelineViewportStateCreateInfo CreateViewportState(const vk::Extent2D& swapChainExtent)
    {
        Pipeline::viewport = vk::Viewport()
            .setX(0.f).setY(0.f)
            .setWidth((float)swapChainExtent.width).setHeight((float)swapChainExtent.height)
            .setMinDepth(0.f).setMaxDepth(1.f);
        Pipeline::scissor = vk::Rect2D()
            .setOffset({ 0, 0 })
            .setExtent(swapChainExtent);

        return vk::PipelineViewportStateCreateInfo()
            .setViewportCount(1)
            .setPViewports(&viewport)
            .setScissorCount(1)
            .setPScissors(&scissor);
    }
    
    vk::PipelineInputAssemblyStateCreateInfo CreateInputAssemblyState()
    {
        return vk::PipelineInputAssemblyStateCreateInfo()
            .setTopology(vk::PrimitiveTopology::eTriangleList)
            .setPrimitiveRestartEnable(false);
    }

    vk::PipelineRasterizationStateCreateInfo CreateRasterizerState()
    {
        return vk::PipelineRasterizationStateCreateInfo()
            .setDepthClampEnable(false)
            .setRasterizerDiscardEnable(false)
            .setPolygonMode(vk::PolygonMode::eFill)
            .setCullMode(vk::CullModeFlagBits::eBack)
            .setFrontFace(vk::FrontFace::eCounterClockwise)
            .setDepthBiasEnable(false)
            .setDepthBiasConstantFactor(0.f)
            .setDepthBiasClamp(0.f)
            .setDepthBiasSlopeFactor(0.f)
            .setLineWidth(1.f);
    }

    vk::PipelineMultisampleStateCreateInfo CreateMultisampleState()
    {
        return vk::PipelineMultisampleStateCreateInfo()
            .setRasterizationSamples(vk::SampleCountFlagBits::e1)
            .setSampleShadingEnable(false)
            .setMinSampleShading(0.f)
            .setPSampleMask(nullptr)
            .setAlphaToCoverageEnable(false)
            .setAlphaToOneEnable(false);
    }

    std::tuple<vk::ShaderModule, vk::PipelineShaderStageCreateInfo> CreateShaderStage(vk::Device device, const std::string& path, vk::ShaderStageFlagBits stage)
    {
        const auto code = ReadFile(path);
        const auto moduleInfo = vk::ShaderModuleCreateInfo()
            .setCodeSize(code.size())
            .setPCode(reinterpret_cast<const u32*>(code.data()));
        const auto module = device.createShaderModule(moduleInfo);
        return std::tie(
            module,
            vk::PipelineShaderStageCreateInfo().setStage(stage).setModule(module).setPName("main")
        );
    }

    vk::PipelineColorBlendAttachmentState CreateColorBlendAttachmentState()
    {
        return vk::PipelineColorBlendAttachmentState()
            .setBlendEnable(false)
            .setSrcColorBlendFactor(vk::BlendFactor::eZero)
            .setDstColorBlendFactor(vk::BlendFactor::eZero)
            .setColorBlendOp(vk::BlendOp::eAdd)
            .setSrcAlphaBlendFactor(vk::BlendFactor::eZero)
            .setDstAlphaBlendFactor(vk::BlendFactor::eZero)
            .setAlphaBlendOp(vk::BlendOp::eAdd)
            .setColorWriteMask(vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);
    }

    vk::PipelineColorBlendStateCreateInfo CreateColorBlendState()
    {
        Pipeline::colorBlendAttachment = Pipeline::CreateColorBlendAttachmentState();

        return vk::PipelineColorBlendStateCreateInfo()
            .setLogicOpEnable(false)
            .setLogicOp(vk::LogicOp::eCopy)
            .setAttachmentCount(1)
            .setPAttachments(&colorBlendAttachment)
            .setBlendConstants({ 0.f, 0.f, 0.f, 0.f });
    }

    vk::PipelineLayout CreatePipelineLayout(vk::Device device, vk::DescriptorSetLayout* descLayout)
    {
        const auto info = vk::PipelineLayoutCreateInfo()
            .setSetLayoutCount(1)
            .setPSetLayouts(descLayout)
            .setPushConstantRangeCount(0)
            .setPPushConstantRanges(nullptr);
        return device.createPipelineLayout(info);
    }

    vk::Pipeline Create(vk::Device device,
                        const std::vector<vk::PipelineShaderStageCreateInfo>& shaderStages, 
                        const vk::PipelineVertexInputStateCreateInfo& vertexInput,
                        const vk::PipelineInputAssemblyStateCreateInfo& inputAssembly,
                        const vk::PipelineViewportStateCreateInfo& viewport,
                        const vk::PipelineRasterizationStateCreateInfo& rasterizer,
                        const vk::PipelineMultisampleStateCreateInfo& multisampling,
                        const vk::PipelineColorBlendStateCreateInfo& colorBlending,
                        vk::PipelineLayout layout,
                        vk::RenderPass renderPass)
    {
        const auto info = vk::GraphicsPipelineCreateInfo()
            .setStageCount((u32)shaderStages.size())
            .setPStages(shaderStages.data())
            .setPVertexInputState(&vertexInput)
            .setPInputAssemblyState(&inputAssembly)
            .setPViewportState(&viewport)
            .setPRasterizationState(&rasterizer)
            .setPMultisampleState(&multisampling)
            .setPColorBlendState(&colorBlending)
            .setLayout(layout)
            .setRenderPass(renderPass)
            .setBasePipelineHandle(nullptr)
            .setBasePipelineIndex(-1);

        return device.createGraphicsPipeline(nullptr, info);
    }
}

void Application::CreateGraphicsPipeline()
{
    const auto vertexInput   = Pipeline::CreateVertexInputState();
    const auto inputAssembly = Pipeline::CreateInputAssemblyState();
    const auto viewportState = Pipeline::CreateViewportState(m_swapChain->GetExtent());
    const auto rasterizer    = Pipeline::CreateRasterizerState();
    const auto multisampling = Pipeline::CreateMultisampleState();
    const auto colorBlending = Pipeline::CreateColorBlendState();

    m_pipelineLayout = Pipeline::CreatePipelineLayout(*m_device, &m_descriptorSetLayout);

    const auto [vertShaderModule, vertStageInfo] = Pipeline::CreateShaderStage(*m_device, "resources/shaders/vert.spv", vk::ShaderStageFlagBits::eVertex);
    const auto [fragShaderModule, fragStageInfo] = Pipeline::CreateShaderStage(*m_device, "resources/shaders/frag.spv", vk::ShaderStageFlagBits::eFragment);

    m_graphicsPipeline = Pipeline::Create(*m_device, 
                                          { vertStageInfo, fragStageInfo }, 
                                          vertexInput, inputAssembly, viewportState, rasterizer, multisampling, colorBlending, 
                                          m_pipelineLayout, m_renderPass->GetHandle());

    m_device->destroyShaderModule(vertShaderModule);
    m_device->destroyShaderModule(fragShaderModule);
}

void Application::CreateFramebuffers()
{
    const size_t fbCount = m_swapChain->GetImageCount();
    m_swapChainFramebuffers.resize(fbCount);
    for (size_t i = 0; i < fbCount; ++i)
    {
        const auto fbInfo = vk::FramebufferCreateInfo()
            .setRenderPass(m_renderPass->GetHandle())
            .setAttachmentCount(1)
            .setPAttachments(&m_swapChain->GetImageViewAtIndex(i))
            .setWidth(m_swapChain->GetExtentWidth())
            .setHeight(m_swapChain->GetExtentHeight())
            .setLayers(1);

        m_swapChainFramebuffers[i] = m_device->createFramebuffer(fbInfo);
    }
}

std::tuple<vk::Buffer, vk::DeviceMemory> Application::CreateBuffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties)
{
    const auto bufferInfo = vk::BufferCreateInfo()
        .setSize(size)
        .setUsage(usage)
        .setSharingMode(vk::SharingMode::eExclusive);
    vk::Buffer buffer = m_device->createBuffer(bufferInfo);

    const auto memRequirements = m_device->getBufferMemoryRequirements(buffer);
    const auto memAllocInfo = vk::MemoryAllocateInfo()
        .setAllocationSize(memRequirements.size)
        .setMemoryTypeIndex(m_physicalDevice->FindMemoryType(memRequirements.memoryTypeBits, properties));
    vk::DeviceMemory bufferMemory = m_device->allocateMemory(memAllocInfo);

    const u32 memoryOffset = 0;
    m_device->bindBufferMemory(buffer, bufferMemory, memoryOffset);

    return std::make_tuple(buffer, bufferMemory);
}

void Application::CopyBuffer(vk::Buffer source, vk::Buffer dest, vk::DeviceSize size)
{
    // TODO: command pool for transfer operations 
    
    // Allocate command buffer 
    const auto cmdBufferInfo = vk::CommandBufferAllocateInfo()
        .setLevel(vk::CommandBufferLevel::ePrimary)
        .setCommandPool(m_cmdPool)
        .setCommandBufferCount(1);
    auto cmdBuffer = m_device->allocateCommandBuffers(cmdBufferInfo)[0];

    // Record command buffer
    const vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    cmdBuffer.begin(beginInfo);

    auto copyRegion = vk::BufferCopy()
        .setSrcOffset(0)
        .setDstOffset(0)
        .setSize(size);
    cmdBuffer.copyBuffer(source, dest, { copyRegion });
    
    cmdBuffer.end();

    // Execute command buffer 
    const auto submitInfo = vk::SubmitInfo()
        .setCommandBufferCount(1)
        .setPCommandBuffers(&cmdBuffer);
    const vk::Fence noFence = nullptr;
    m_graphicsQueue.submit({ submitInfo }, noFence);
    m_graphicsQueue.waitIdle();

    // Clean up command buffer 
    m_device->freeCommandBuffers(m_cmdPool, { cmdBuffer });
}

void Application::CreateVertexBuffer()
{
    const vk::DeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();
    
    auto [stagingBuffer, stagingMemory] = CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
    
    const int memoryOffset = 0;     
    const vk::MemoryMapFlags noMapMemoryFlags;
    void* data = nullptr;
    m_device->mapMemory(stagingMemory, memoryOffset, bufferSize, noMapMemoryFlags, &data);
    memcpy(data, vertices.data(), static_cast<size_t>(bufferSize));
    m_device->unmapMemory(stagingMemory);

    std::tie(m_vertexBuffer, m_vertexBufferMemory) = CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);
    CopyBuffer(stagingBuffer, m_vertexBuffer, bufferSize);

    // Destroy temporary staging buffer 
    m_device->destroyBuffer(stagingBuffer);
    m_device->freeMemory(stagingMemory);
}

void Application::CreateIndexBuffer()
{
    const vk::DeviceSize bufferSize = sizeof(indices[0]) * indices.size();

    auto [stagingBuffer, stagingMemory] = CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    const int memoryOffset = 0;
    const vk::MemoryMapFlags noMapMemoryFlags;
    void* data;
    m_device->mapMemory(stagingMemory, memoryOffset, bufferSize, noMapMemoryFlags, &data);
    memcpy(data, indices.data(), static_cast<size_t>(bufferSize));
    m_device->unmapMemory(stagingMemory);

    std::tie(m_indexBuffer, m_indexBufferMemory) = CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);
    CopyBuffer(stagingBuffer, m_indexBuffer, bufferSize);

    // Destroy temporary staging buffer 
    m_device->destroyBuffer(stagingBuffer);
    m_device->freeMemory(stagingMemory);
}

void Application::CreateUniformBuffers()
{
    const auto bufferSize = sizeof(UniformBufferObject);

    for (size_t i = 0; i < m_uniformBuffers.size(); ++i)
    {
        std::tie(m_uniformBuffers[i], m_uniformBuffersMemory[i]) = 
            CreateBuffer(sizeof(UniformBufferObject), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
    }
}

void Application::CreateDescriptorPool()
{
    const auto frameCount = static_cast<u32>(m_swapChain->GetImageCount());
    const auto descPoolSize = vk::DescriptorPoolSize()
        .setDescriptorCount(frameCount);
    const auto poolInfo = vk::DescriptorPoolCreateInfo()
        .setPoolSizeCount(1)
        .setPPoolSizes(&descPoolSize)
        .setMaxSets(frameCount);
    m_descriptorPool = m_device->createDescriptorPool(poolInfo);
}

void Application::CreateDescriptorSets()
{
    const auto frameCount = static_cast<u32>(m_swapChain->GetImageCount());
    const std::vector<vk::DescriptorSetLayout> layouts(frameCount, m_descriptorSetLayout);
    const auto allocInfo = vk::DescriptorSetAllocateInfo()
        .setDescriptorPool(m_descriptorPool)
        .setDescriptorSetCount(frameCount)
        .setPSetLayouts(layouts.data());

    m_descriptorSets = m_device->allocateDescriptorSets(allocInfo);

    for (size_t i = 0; i < frameCount; i++) 
    {
        const auto bufferInfo = vk::DescriptorBufferInfo()
            .setBuffer(m_uniformBuffers[i])
            .setOffset(0)
            .setRange(sizeof(UniformBufferObject));

        const auto writeDescriptor = vk::WriteDescriptorSet()
            .setDstSet(m_descriptorSets[i])
            .setDstBinding(0)
            .setDstArrayElement(0)
            .setDescriptorType(vk::DescriptorType::eUniformBuffer)
            .setDescriptorCount(1)
            .setPBufferInfo(&bufferInfo);

        m_device->updateDescriptorSets({ writeDescriptor }, {});
    }
}

void Application::CreateCmdPool()
{
    const auto indices = m_physicalDevice->GetQueueFamilyIndices();
    const auto poolInfo = vk::CommandPoolCreateInfo()
        .setQueueFamilyIndex(indices.graphicsFamily.value());
    m_cmdPool = m_device->createCommandPool(poolInfo);
}

void Application::CreateCmdBuffers()
{
    ///////////////
    // Allocate 
    ///////////////
    m_cmdBuffers.resize(m_swapChainFramebuffers.size());

    const auto allocInfo = vk::CommandBufferAllocateInfo()
        .setCommandPool(m_cmdPool)
        .setLevel(vk::CommandBufferLevel::ePrimary)
        .setCommandBufferCount(static_cast<u32>(m_cmdBuffers.size()));
    m_cmdBuffers = m_device->allocateCommandBuffers(allocInfo);

    //////////////
    // Record
    //////////////
    for (size_t i = 0; i < m_cmdBuffers.size(); ++i) 
    {
        auto& buffer = m_cmdBuffers[i];

        const auto beginInfo = vk::CommandBufferBeginInfo()
            .setFlags(vk::CommandBufferUsageFlagBits::eSimultaneousUse);
        
        buffer.begin(beginInfo);

        const std::array<float, 4> clearColorValue{ 0.f, 0.f, 0.f, 1.f };
        const vk::ClearValue clearColor(clearColorValue);
        const auto renderArea = vk::Rect2D()
            .setOffset({ 0, 0 })
            .setExtent(m_swapChain->GetExtent());

        const auto renderPassInfo = vk::RenderPassBeginInfo()
            .setRenderPass(m_renderPass->GetHandle())
            .setFramebuffer(m_swapChainFramebuffers[i])
            .setRenderArea(renderArea)
            .setClearValueCount(1)
            .setPClearValues(&clearColor);

        buffer.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

        // Bind graphics pipeline 
        buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_graphicsPipeline);
    
        const u32 firstBinding = 0;
        const u32 numVertBuffers = 1;
        const vk::Buffer vertBuffers[] = { m_vertexBuffer };
        const vk::DeviceSize offsets[] = { 0 };
        buffer.bindVertexBuffers(firstBinding, numVertBuffers, vertBuffers, offsets);
        const u32 indexBufferOffset = 0;
        buffer.bindIndexBuffer(m_indexBuffer, indexBufferOffset, vk::IndexType::eUint16);

        // Draw 
        /*const u32 vertCount = vertices.size();
        const u32 firstVertex = 0;
        const u32 instanceCount = 1;
        const u32 firstInstance = 0;
        buffer.draw(vertCount, instanceCount, firstVertex, firstInstance);
        */
        buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout, 0, { m_descriptorSets[i] }, {});
        const u32 indexCount = indices.size();
        const u32 instanceCount = 1;
        const u32 firstIndex = 0;
        const u32 firstInstance = 0;
        const u32 vertOffset = 0;
        buffer.drawIndexed(indexCount, instanceCount, firstIndex, vertOffset, firstInstance);

        // End render pass
        buffer.endRenderPass();

        // End recording 
        buffer.end();
    }
}

void Application::CreateSyncObjects()
{
    for (size_t i = 0; i < m_maxFramesInFlight; ++i)
    {
        m_imageAvailableSemaphores[i] = m_device->createSemaphore({});
        m_renderFinishedSemaphores[i] = m_device->createSemaphore({});
        m_inFlightFences[i] = m_device->createFence({ vk::FenceCreateFlagBits::eSignaled });
    }
}

void Application::CreateLogicalDevice()
{
    const auto indices = m_physicalDevice->GetQueueFamilyIndices();
    
    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    std::set<u32> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

    float queuePriorities[] = { 1.f };
    queueCreateInfos.reserve(uniqueQueueFamilies.size());
    for (u32 queueFamily : uniqueQueueFamilies)
    {
        const auto createInfo = vk::DeviceQueueCreateInfo()
            .setQueueFamilyIndex(queueFamily)
            .setQueueCount(1)
            .setPQueuePriorities(queuePriorities);
        queueCreateInfos.emplace_back(std::move(createInfo));
    }

    m_device = std::make_shared<vk::Device>(m_physicalDevice->CreateLogicalDevice(queueCreateInfos));

    m_graphicsQueue = m_device->getQueue(indices.graphicsFamily.value(), 0);
    m_presentQueue = m_device->getQueue(indices.presentFamily.value(), 0);
}

void Application::RunLoop()
{
    while (!glfwWindowShouldClose(m_pWindow))
    {
        glfwPollEvents();
        //if (shouldDraw)        // Don't draw if glfw triggers the "iconified" event (driver issues) 
        DrawFrame();
    }
    
    m_device->waitIdle();
}

std::optional<u32> Application::GetNextSwapChainImageIndex()
{
    const vk::SwapchainKHR swapChainHandle = m_swapChain->GetHandle();
    constexpr u64 timeout = std::numeric_limits<u64>::max(); // No timeout
    const vk::Semaphore syncSemaphore = m_imageAvailableSemaphores[m_currentFrame];
    const vk::Fence syncFence = nullptr;

    auto [result, imageIndex] = m_device->acquireNextImageKHR(swapChainHandle, timeout, syncSemaphore, syncFence);
    
    if (result == vk::Result::eErrorOutOfDateKHR ||
        result == vk::Result::eSuboptimalKHR ||
        m_framebufferResized)
    {
        m_framebufferResized = false;
        RecreateSwapchain();
        return std::nullopt;
    }
    else if (result != vk::Result::eSuccess)
    {
        throw std::runtime_error("DrawFrame(): Failed to acquire swap chain image!");
    }

    return imageIndex;
}

void Application::DrawFrame()
{
    m_device->waitForFences({ m_inFlightFences[m_currentFrame] }, true, std::numeric_limits<u64>::max());

    const auto imageIndex = GetNextSwapChainImageIndex();
    if (!imageIndex.has_value())
        return;
    const vk::CommandBuffer commandBuffers[] = {
        m_cmdBuffers[*imageIndex]
    };
    const u32 imageIndexValues[] = {
        *imageIndex
    };

    UpdateUniformBuffer(*imageIndex);

    // Submit queue 
    const vk::Semaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
    const vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
    const vk::Semaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
    auto submitInfo = vk::SubmitInfo(
        /* Wait Semaphore Count */     1
        /* Wait Semaphores */          , waitSemaphores
        /* Wait Dst Stage Flags */     , waitStages

        /* Cmd Buffer Count */         , 1
        /* Cmd Buffers */              , commandBuffers

        /* Signal Semaphore Count */   , 1
        /* Signal Semaphores */        , signalSemaphores
    );

    const vk::Fence currentFrameFence = m_inFlightFences[m_currentFrame];
    m_device->resetFences(currentFrameFence);
    m_graphicsQueue.submit({ submitInfo }, currentFrameFence);

    const auto presentInfo = vk::PresentInfoKHR()
        .setWaitSemaphoreCount(1)
        .setPWaitSemaphores(signalSemaphores)
        .setSwapchainCount(1)
        .setPSwapchains(&m_swapChain->GetHandle())
        .setPImageIndices(imageIndexValues);

    // Submit request to present an image to the swap chain 
    m_presentQueue.presentKHR(presentInfo);

    // Wait for work to finish 
    m_presentQueue.waitIdle();

    m_currentFrame = (m_currentFrame + 1) & m_maxFramesInFlight;
}

void Application::RecreateSwapchain()
{
    m_windowWidth = 0;
    m_windowHeight = 0;
    while (m_windowWidth == 0 ||
           m_windowHeight == 0)
    {
        glfwGetFramebufferSize(m_pWindow, &m_windowWidth, &m_windowHeight);
        glfwWaitEvents();
    }

    // Resources may still be in use 
    m_device->waitIdle();

    DestroySwapChain();

    m_swapChain = std::unique_ptr<SwapChain>(new SwapChain(m_device, m_physicalDevice->GetHandle(), m_surface, m_pWindow));
    m_renderPass = std::make_unique<RenderPass>(m_device, m_swapChain->GetImageFormat());
    CreateGraphicsPipeline();
    CreateFramebuffers();
    CreateUniformBuffers();
    CreateDescriptorPool();
    CreateDescriptorSets();
    CreateCmdBuffers();
}

void Application::DestroySwapChain()
{
    for (auto& framebuffer : m_swapChainFramebuffers)
        m_device->destroyFramebuffer(framebuffer);

    m_device->freeCommandBuffers(m_cmdPool, m_cmdBuffers);
    
    m_device->destroyPipeline(m_graphicsPipeline);
    m_device->destroyPipelineLayout(m_pipelineLayout);
    
    for (auto& buffer : m_uniformBuffers)
        m_device->destroyBuffer(buffer);

    for (auto& bufferMemory : m_uniformBuffersMemory)
        m_device->freeMemory(bufferMemory);

    m_device->destroyDescriptorPool(m_descriptorPool);

    m_renderPass.reset();
    m_swapChain.reset();
}

void Application::Cleanup()
{
    DestroySwapChain();

    m_device->destroyDescriptorSetLayout(m_descriptorSetLayout);

    m_device->destroyBuffer(m_vertexBuffer);
    m_device->freeMemory(m_vertexBufferMemory);
    
    m_device->destroyBuffer(m_indexBuffer);
    m_device->freeMemory(m_indexBufferMemory);

    for (size_t i = 0; i < m_maxFramesInFlight; ++i)
    {
        m_device->destroySemaphore(m_imageAvailableSemaphores[i]);
        m_device->destroySemaphore(m_renderFinishedSemaphores[i]);
        m_device->destroyFence(m_inFlightFences[i]);
    }

    m_device->destroy();
    m_instance.reset();

    glfwDestroyWindow(m_pWindow);
    glfwTerminate();
}

//void Application::OnWindowMinimize(GLFWwindow* window, int minimized)
//{
//    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
//    
//    if (minimized)
//    {
//        app->m_shouldDraw = false;
//    }
//    else // Restored 
//    {
//        app->m_shouldDraw = true;
//    }
//}

void Application::OnWindowFramebufferResize(GLFWwindow* window, int width, int height)
{
    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
    app->m_framebufferResized = true;
}

void Application::UpdateUniformBuffer(u32 currentImage)
{
    static auto startTime = std::chrono::high_resolution_clock::now();
    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    UniformBufferObject ubo = {};
    ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.proj = glm::perspective(glm::radians(45.0f), m_swapChain->GetExtentWidth() / (float)m_swapChain->GetExtentHeight(), 0.1f, 10.0f);
    ubo.proj[1][1] *= -1;

    void* data = m_device->mapMemory(m_uniformBuffersMemory[currentImage], 0, sizeof(ubo));
    memcpy(data, &ubo, sizeof(ubo));
    m_device->unmapMemory(m_uniformBuffersMemory[currentImage]);
}
