#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/component_wise.hpp>
#include <glm/gtx/vector_angle.hpp>


// Ellipsoid
// Centered at (0, 0, 0)
// Defined by 3 radii: a, b, c along the x-, y-, and z- axes respectively 
// Implicit eqn of the ellipsoid: 
//   For a point belonging to the surface (xs, ys, zs):
//   xs^2/a^2 + ys^2/b^2 + zs^2/c^2 = 1

// Oblate spheroid: two of the radii are equal (semi-major axes); the third is less than either (semi-minor axis)


// All storage/operations in radians 
// (w/ human-readable conversions to/from degrees)

// (long, lat) point on surface 
class Geodetic2
{
public:
    // In degrees; performs conversion to rad internally 
    // TODO: user literals for units 
    explicit Geodetic2(double longitude, double latitude);
    // Use copy ctor for assigning rad directly 
    Geodetic2(const Geodetic2& other) = default;

    // In radians 
    double GetLongitude() const { return m_longitude; }

    // In radians 
    double GetLatitude() const { return m_latitude; }

private:
    // Stored in radians; constructed in degrees (or radians by copy of a degree-constructed object)
    double m_longitude;    // [-180, +180]; 0 at prime meridian, +ve going east 
    double m_latitude;     // [-90, 90]; 0 at ecuator, +ve going north
};

// (long, lat, height) at height away from the surface point (long, lat)
class Geodetic3
{
public:
    // Surface coords in degrees; performs conversion to rad internally 
    // Geodetic height in metres
    // TODO: user literals for units 
    explicit Geodetic3(double longitude, double latitude, double height);
    explicit Geodetic3(const Geodetic2& surfaceCoord, double height);
    // Use copy ctor for assigning rad directly 
    Geodetic3(const Geodetic3& other) = default;

    // In radians 
    double GetLongitude() const { return m_surfacePoint.GetLongitude(); }
    
    // In radians 
    double GetLatitude() const { return m_surfacePoint.GetLatitude(); }
    
    // In metres 
    double GetHeight() const { return m_height; }

private:
    Geodetic2 m_surfacePoint;
    double m_height;
};

class Ellipsoid
{
public:
    // Very large space -> require high precision 
    // All units for Cartesian coords and geodetic coords are in metres 
    explicit Ellipsoid(double xRadius, double yRadius, double zRadius);
    explicit Ellipsoid(const glm::dvec3& radii);

    const glm::dvec3& GetRadii() const;

    // Known surface point in Cartesian cords (x, y, z)
    glm::dvec3 ComputeGeodeticSurfaceNormal(const glm::dvec3& atSurfacePoint) const;

    // Known surface point in geographic coords (long, lat, height)
    const glm::dvec3 ComputeGeodeticSurfaceNormal(const Geodetic3& geodetic) const;

    // Geographic (long, lat, height) to WGS84 (x, y, z)
    const glm::dvec3 GeodeticToCartesian(const Geodetic3& geodetic) const;

    Geodetic2 CartesianToGeodetic2(const glm::dvec3& surfacePoint) const;
    glm::dvec3 ScaleToGeocentricSurface(const glm::dvec3& point) const;
    glm::dvec3 ScaleToGeodeticSurface(const glm::dvec3& p) const;
    Geodetic3 CartesianToGeodetic3(glm::dvec3& pos) const;

    std::vector<glm::dvec3> ComputeCurve(const glm::dvec3& start, const glm::dvec3& end, double granularity)
    {
        const glm::dvec3 normal = glm::normalize(glm::cross(start, end));
        const double theta = glm::angle(start, end);
        const int s = glm::max(static_cast<int>(theta / granularity) - 1, 0);
        
        std::vector<glm::dvec3> points(2 + s); // Include start & end
        points.push_back(start);
        for (int i = 0; i < s; ++i)
        {
            const double phi = i * granularity;
            const glm::dvec3 rotated = glm::rotate(start, phi, normal);
            points.emplace_back(std::move(rotated));
        }
        points.push_back(end);

        return points;
    }

private:
    glm::dvec3 m_radii;
    glm::dvec3 m_oneOverRadiiSq; // (1/a^2, 1/b^2, 1/c^2)
    glm::dvec3 m_radiiSq;        // (a^2, b^2, c^2)
    glm::dvec3 m_radiiToFourth;  // (a^4, b^4, c^4)
};

const Ellipsoid g_unitSphere(
      1.0
    , 1.0
    , 1.0
);

// WGS84 coords 
// In metres 
const Ellipsoid g_wgs84(
    6378137.0
  , 6378137.0
  , 6356752.314245
);




