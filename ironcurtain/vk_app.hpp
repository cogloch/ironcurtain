#pragma once
#include <vulkan/vulkan.hpp>
//#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "swap_chain.hpp"
#include "renderpass.hpp"
#include "vk_instance.hpp"
#include "vk_physical_device.hpp"
#include "vk_utils.hpp"


class Application
{
public:
    Application();

private:
    //////////////////////
    // INIT
    //////////////////////
    void InitWindow();

    void InitVulkan();

    void CreateLogicalDevice();

    void CreateDescriptorSetLayout();
    void CreateGraphicsPipeline();

    void CreateFramebuffers();

    std::tuple<vk::Buffer, vk::DeviceMemory> CreateBuffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties);
    void CopyBuffer(vk::Buffer source, vk::Buffer dest, vk::DeviceSize size);
    void CreateVertexBuffer();
    void CreateIndexBuffer();
    void CreateUniformBuffers();
    void CreateDescriptorPool();
    void CreateDescriptorSets();

    void CreateCmdPool();
    void CreateCmdBuffers();

    void CreateSyncObjects();

    //////////////////////
    // MAIN
    //////////////////////
    void RunLoop();
    std::optional<u32> GetNextSwapChainImageIndex();
    void DrawFrame();

    void RecreateSwapchain();

    //////////////////////
    // DESTROY
    //////////////////////
    void DestroySwapChain();
    void Cleanup();

    GLFWwindow* m_pWindow = nullptr;
    int m_windowWidth = 800;
    int m_windowHeight = 600;

    std::unique_ptr<VulkanInstance> m_instance;             
    vk::SurfaceKHR                  m_surface;             // Presentation surface 
    std::shared_ptr<vk::Device>     m_device;              // Logical device 
    std::unique_ptr<VulkanPhysicalDevice> m_physicalDevice;
    vk::Queue                       m_graphicsQueue;       // Graphics queue 
    vk::Queue                       m_presentQueue;        // Presentation queue

    std::unique_ptr<SwapChain>      m_swapChain;
    std::unique_ptr<RenderPass>     m_renderPass;

    vk::DescriptorSetLayout         m_descriptorSetLayout;
    vk::PipelineLayout              m_pipelineLayout;
    vk::Pipeline                    m_graphicsPipeline;
    std::vector<vk::Framebuffer>    m_swapChainFramebuffers;

    vk::CommandPool m_cmdPool;
    std::vector<vk::CommandBuffer> m_cmdBuffers;

    size_t m_currentFrame = 0;
    // One set of sync primitives for each frame 
    static const size_t m_maxFramesInFlight = 2;
    // GPU-GPU
    std::array<vk::Semaphore, m_maxFramesInFlight> m_imageAvailableSemaphores;
    std::array<vk::Semaphore, m_maxFramesInFlight> m_renderFinishedSemaphores;
    // CPU-GPU
    std::array<vk::Fence, m_maxFramesInFlight>     m_inFlightFences;

    //bool m_shouldDraw = true;
    //static void OnWindowMinimize(GLFWwindow* window, int minimized);
    
    bool m_framebufferResized = false;
    static void OnWindowFramebufferResize(GLFWwindow* window, int width, int height);

    vk::Buffer       m_vertexBuffer;
    vk::DeviceMemory m_vertexBufferMemory;

    vk::Buffer       m_indexBuffer;
    vk::DeviceMemory m_indexBufferMemory;

    void UpdateUniformBuffer(u32 currentImage);

    std::array<vk::Buffer, m_maxFramesInFlight>       m_uniformBuffers;
    std::array<vk::DeviceMemory, m_maxFramesInFlight> m_uniformBuffersMemory;

    vk::DescriptorPool m_descriptorPool;
    std::vector<vk::DescriptorSet> m_descriptorSets;
};


struct Vertex
{
    glm::vec2 pos;
    glm::vec3 color;

    static vk::VertexInputBindingDescription GetBindingDescription()
    {
        return vk::VertexInputBindingDescription(
            /* Binding */         0,
            /* Stride */          sizeof(Vertex), 
            /* Input Rate */      vk::VertexInputRate::eVertex
        );
    }

    static std::array<vk::VertexInputAttributeDescription, 2> GetAttributeDescriptions()
    {
        std::array<vk::VertexInputAttributeDescription, 2> descs;
        
        descs[0] = vk::VertexInputAttributeDescription(
            /* Location */     0,
            /* Binding */      0,
            /* Format */       vk::Format::eR32G32Sfloat,
            /* Offset */       offsetof(Vertex, pos)
        );
        
        descs[1] = vk::VertexInputAttributeDescription(
            /* Location */     1,
            /* Binding */      0,
            /* Format */       vk::Format::eR32G32B32Sfloat,
            /* Offset */       offsetof(Vertex, color)
        );


        return descs;
    }
};

const std::vector<Vertex> vertices = {
    {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
    {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
};

const std::vector<uint16_t> indices = {
    0, 1, 2, 2, 3, 0
};

struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
};






