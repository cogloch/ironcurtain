#include "pch.hpp"
#include "globe_geometry.hpp"

Ellipsoid::Ellipsoid(double xRadius, double yRadius, double zRadius)
    : Ellipsoid(glm::dvec3(xRadius, yRadius, zRadius))
{
}

Ellipsoid::Ellipsoid(const glm::dvec3& radii)
    : m_radii(radii)
{
    m_oneOverRadiiSq = glm::dvec3(
          1 / (m_radii.x * m_radii.x)
        , 1 / (m_radii.y * m_radii.y)
        , 1 / (m_radii.z * m_radii.z)
    );

    m_radiiSq = glm::dvec3(
          m_radii.x * m_radii.x
        , m_radii.y * m_radii.y
        , m_radii.z * m_radii.z
    );

    m_radiiToFourth = glm::dvec3(
          m_radiiSq.x * m_radiiSq.x
        , m_radiiSq.y * m_radiiSq.y
        , m_radiiSq.z * m_radiiSq.z
    );
}

Geodetic2::Geodetic2(double longitude, double latitude)
    : m_longitude(glm::radians(longitude))
    , m_latitude(glm::radians(latitude))
{
}

Geodetic3::Geodetic3(double longitude, double latitude, double height)
    : m_surfacePoint(longitude, latitude)
    , m_height(height)
{
}

Geodetic3::Geodetic3(const Geodetic2& surfaceCoord, double height)
    : m_surfacePoint(surfaceCoord)
    , m_height(height)
{
}

const glm::dvec3& Ellipsoid::GetRadii() const
{
    return m_radii;
}

// Known surface point in Cartesian cords (x, y, z)
glm::dvec3 Ellipsoid::ComputeGeodeticSurfaceNormal(const glm::dvec3& atSurfacePoint) const
{
    // Component-wise multiplication: a/xradius^2, etc. 
    return glm::normalize(atSurfacePoint * m_oneOverRadiiSq);
}

// Known surface point in geographic coords (long, lat, height)
const glm::dvec3 Ellipsoid::ComputeGeodeticSurfaceNormal(const Geodetic3& geodetic) const
{
    const double longitude = geodetic.GetLatitude();
    const double latitude = geodetic.GetLatitude();
    const double cosLat = glm::cos(latitude);
    return {
          cosLat * glm::cos(longitude)
        , cosLat * glm::sin(longitude)
        , glm::sin(latitude)
    };
}

// Geographic (long, lat, height) to WGS84 (x, y, z)
const glm::dvec3 Ellipsoid::GeodeticToCartesian(const Geodetic3& geodetic) const
{
    const glm::dvec3 n = ComputeGeodeticSurfaceNormal(geodetic);  // Normalised!
    const glm::dvec3 k = m_radiiSq * n;  // Component-wise: (a^2 * n_x, b^2 * n_y, c^2 * n_z)
    const double gamma = glm::sqrt(glm::dot(n, k));
    const glm::dvec3 rSurface = k / gamma;
    return rSurface + (geodetic.GetHeight() * n);
}

Geodetic2 Ellipsoid::CartesianToGeodetic2(const glm::dvec3& surfacePoint) const
{
    const glm::dvec3 n = ComputeGeodeticSurfaceNormal(surfacePoint);
    return Geodetic2(
        atan2(n.y, n.x)                  // latitude                     
        , asin(n.z / glm::length(n))       // longitude
    );
}

glm::dvec3 Ellipsoid::ScaleToGeocentricSurface(const glm::dvec3& point) const
{
    const double beta = 1.0 / glm::sqrt(
        (point.x * point.x) * m_oneOverRadiiSq.x +
        (point.y * point.y) * m_oneOverRadiiSq.y +
        (point.z * point.z) * m_oneOverRadiiSq.z
    );
    return beta * point;
}

glm::dvec3 Ellipsoid::ScaleToGeodeticSurface(const glm::dvec3& p) const
{
    const double beta = 1.0 / glm::sqrt(
        (p.x * p.x) * m_oneOverRadiiSq.x +
        (p.y * p.y) * m_oneOverRadiiSq.y +
        (p.z * p.z) * m_oneOverRadiiSq.z
    );

    const double n = glm::length(glm::dvec3(
        beta * p.x * m_oneOverRadiiSq.x
        , beta * p.y * m_oneOverRadiiSq.y
        , beta * p.z * m_oneOverRadiiSq.z
    ));

    double alpha = (1.0 - beta) * (glm::length(p) / n);

    const glm::dvec3 p2(
        p.x * p.x
        , p.y * p.y
        , p.z * p.z
    );

    glm::dvec3 dp(0.0, 0.0, 0.0);
    double s = 0.0;
    double dSdA = 1.0;  // dS/dA

    // Newton-Rhapson
    do
    {
        alpha -= (s / dSdA);

        dp = {
              1.0 + (alpha * m_oneOverRadiiSq.x)
            , 1.0 + (alpha * m_oneOverRadiiSq.y)
            , 1.0 + (alpha * m_oneOverRadiiSq.z)
        };

        // dp^2
        const glm::dvec3 dp2(
            dp.x * dp.x,
            dp.y * dp.y,
            dp.z * dp.z
        );

        // dp^3
        const glm::dvec3 dp3(
            dp2.x * dp.x,
            dp2.y * dp.y,
            dp2.z * dp.z
        );

        s = p2.x / (m_radiiSq.x * dp2.x) +
            p2.y / (m_radiiSq.y * dp2.y) +
            p2.z / (m_radiiSq.z * dp2.z) - 1.0;

        dSdA = -2.0 * (
            p2.x / (m_radiiToFourth.x * dp3.x) +
            p2.y / (m_radiiToFourth.y * dp3.y) +
            p2.z / (m_radiiToFourth.z * dp3.z)
            );
    } while (abs(s) > 1e-10);

    return {
          p.x / dp.x
        , p.y / dp.y
        , p.z / dp.z
    };
}

Geodetic3 Ellipsoid::CartesianToGeodetic3(glm::dvec3& pos) const
{
    const glm::dvec3 p = ScaleToGeodeticSurface(pos);
    const glm::dvec3 h = pos - p;
    const double height = glm::sign(glm::dot(h, pos)) * glm::length(h);
    return Geodetic3(CartesianToGeodetic2(p), height);
}
