#pragma once
#include <vulkan/vulkan.hpp>


struct GLFWwindow;

class VulkanInstance
{
public:
    VulkanInstance();
    ~VulkanInstance();

    vk::SurfaceKHR GetSurface(GLFWwindow* forWindow) const;
    std::vector<vk::PhysicalDevice> EnumeratePhysicalDevices() const;

private:
    vk::Instance m_vkHandle;
    
    // Multiple vkInstance objects not allowed. 
    static bool s_instantiated;
};