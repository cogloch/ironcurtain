#include "pch.hpp"
#include "vk_instance.hpp"
#include <GLFW/glfw3.h>


bool VulkanInstance::s_instantiated = false;

std::vector<const char*> GetRequiredExtensions()
{
    return {
        "VK_KHR_surface",
        "VK_KHR_win32_surface"
    };
}

std::vector<const char*> GetRequiredLayers()
{
    return {
#ifdef _DEBUG
        "VK_LAYER_KHRONOS_validation"
#endif 
    };
}

VulkanInstance::VulkanInstance()
{
    if (s_instantiated)
        throw "Attempting to instantiate multiple vkInstance objects!";

    const auto extensions = GetRequiredExtensions();
    const auto layers = GetRequiredLayers();
    const auto instanceInfo = vk::InstanceCreateInfo()
        .setEnabledLayerCount(layers.size())
        .setPpEnabledLayerNames(layers.data())
        .setEnabledExtensionCount(extensions.size())
        .setPpEnabledExtensionNames(extensions.data());
    m_vkHandle = vk::createInstance(instanceInfo);

    s_instantiated = true;
}

VulkanInstance::~VulkanInstance()
{
    m_vkHandle.destroy();
    s_instantiated = false;
}

vk::SurfaceKHR VulkanInstance::GetSurface(GLFWwindow* forWindow) const
{
    VkSurfaceKHR surface;
    glfwCreateWindowSurface(m_vkHandle, forWindow, nullptr, &surface);
    return vk::SurfaceKHR(surface);
}

std::vector<vk::PhysicalDevice> VulkanInstance::EnumeratePhysicalDevices() const
{
    return m_vkHandle.enumeratePhysicalDevices();
}
