#pragma once
#include <vulkan/vulkan.hpp>


struct QueueFamilyIndices
{
    std::optional<u32> graphicsFamily;
    std::optional<u32> presentFamily;
    bool IsComplete() const { return graphicsFamily.has_value() && presentFamily.has_value(); }
    
    static QueueFamilyIndices FindQueueFamilies(const vk::PhysicalDevice& physDevice, vk::SurfaceKHR surface);
};

