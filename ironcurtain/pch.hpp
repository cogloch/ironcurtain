#pragma once

// C++ stdlib 
#include <set>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <optional>
#include <stdexcept>
#include <algorithm>
#include <functional>

#include "common_types.hpp"